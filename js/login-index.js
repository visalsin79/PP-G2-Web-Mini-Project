$(document).ready(function () {
    var btn_log_out = $("#btn-log-out");
    var btn_log_in = $("#btn-log-in");
    var btn_log_profile = $("#btn-log-profile");
    var login_email = $("#log-in-email");
    var login_pass = $("#log-in-pass");
    var btn_account = $("#btn-account");
    var model_form = $("#modalLRForm");


    var user_list = JSON.parse(localStorage.getItem("useracc"));
    var user_login = localStorage.getItem("login_acc");

    logOutSuccess();
    if (user_login != null) {

        log_in_history(new String(user_login).valueOf());

    }



    btn_log_in.click(function () {

        var email = login_email.val().toLowerCase();

        log_in(email);
    })

    function log_in(email) {
        var chk_email = user_list.map(function (e) { return e.email }).indexOf(email);

        if (chk_email > -1) {
            var chk_password = user_list[chk_email].pass;
            var type = user_list[chk_email].type;

            if (new String(chk_password).valueOf() == new String(login_pass.val()).valueOf()) {
                logInSuccess();
                if (new String(type).valueOf() == new String("work").valueOf()) {
                    btn_log_profile.attr("href", "html/profile-freelancer-editable.html");
                } else {
                    btn_log_profile.attr("href", "html/profile-project-owner-editable.html");
                }
                localStorage.setItem("login_acc", email);
                alert("Successfully")
            } else {
                login_email.addClass('invalid');
                login_pass.addClass('invalid');
            }
        } else {
            login_email.addClass('invalid');
            login_pass.addClass('invalid');
        }
    }

    function log_in_history(email) {
        var chk_email = user_list.map(function (e) { return e.email }).indexOf(email);

        if (chk_email > -1) {

            var type = user_list[chk_email].type;
            logInSuccess();
            if (new String(type).valueOf() == new String("work").valueOf()) {
                btn_log_profile.attr("href", "html/profile-freelancer-editable.html");
            } else {
                btn_log_profile.attr("href", "html/profile-project-owner-editable.html");
            }
            localStorage.setItem("login_acc", email);

        } else {
            login_email.addClass('invalid');
            login_pass.addClass('invalid');
        }
    }
    btn_log_out.click(function () {
        localStorage.removeItem("login_acc")
        logOutSuccess();
    });

    function logInSuccess() {

        btn_log_out.show();
        btn_log_profile.show();
        btn_account.hide();
        model_form.modal('hide');
        login_email.val("").removeClass('valid').siblings().removeClass('active');
        login_pass.val("").removeClass('valid').siblings().removeClass('active');

    }
    function logOutSuccess() {
        btn_log_out.hide();
        btn_log_profile.hide();
        btn_account.show();
    }
})

