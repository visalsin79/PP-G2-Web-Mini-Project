
$(document).ready(function () {
    var list_img = new Array();
    var img = $("#imageInput");
    $("#inputFileUpload").change(function () {
        var img = '<img id="imageInput" src="' + "../img/" + $("#inputFileUpload").val().split('\\').pop() + '" style="height: 100px;">';
        $("#img-collection").append(img);
        list_img.push(img);
    })

    function add_list_image(content) {
        for (var i in list_img) {
            content.append(list_img[i]);
        }
    }


    $("#btnPost").click(function () {

        var cotainer_project = $("#list_projects");
        add_list_image(cotainer_project);

        $("#confirmProjectName").text("");
        $("#confirmCategory").text("");
        $("#confirmRequirment").text("");
        $("#confirmDescription").text("");
        $("#confirmFileUpload").text("");
        $("#confirmProjectDuration").text("");
        $("#confirmBudget").text("");

        $("#confirmProjectName").append(
            $("#inputProjectName").val()
        );
        $("#confirmCategory").append(
            $("#inputCategory option:selected").text()
        );
        var xRequirment = $("#removeConfirmationTags").text();
        var xRequirment2 = xRequirment.replace(/×/g, ", ");
        $("#confirmRequirment").append(
            xRequirment2.slice(0, -2)
        );
        $("#confirmDescription").append(
            $("#inputDescription").val()
        );
        var xfiles = $("#inputFileUpload")[0].files;
        var xFileUpload;
        for (var i = 0; i < xfiles.length; i++) {
            xFileUpload += xfiles[i].name + "<br>";
        }
        $("#confirmFileUpload").append(
            // $("#inputFileUpload").val().replace(/.*(\/|\\)/, '')
            xFileUpload
        );
        $("#confirmProjectDuration").append(
            $("#inputProjectDuration option:selected").text()
        );
        $("#confirmBudget").append(
            $("#inputBudget option:selected").text()
        );
    })
});


function submitForm() {
    $("#fm").submit();
}