$(document).ready(function () {
    pagination(1);

});

var onpage = 1;
var dis = 8;
var lsize = 0;
var lpage = 0;
var arr = [1,2,3,4,5];

function pagination(onpage) {
    displayList(onpage);
}

function paginationActive(onpage) {
    setPage(onpage);
    $(".pagination-num").html("");
    var a;

    for (var i = 0; i < arr.length; i++) {

        if(arr[i]>lpage){
            break;
        }
        if (arr[i] != onpage) {
            a = "";
        } else {
            a = "active";
        }
        $(".pagination-num").append(
            '<li class="page-item ' + a + ' list-inline-item">' +
            '<a class="page-link" onclick="displayList(' + arr[i] + ')">' + arr[i] + '</a>' +
            '</li>'
        )
    }
}

function setPage(onpage){
    if(onpage <5 && arr.indexOf(onpage)==-1){
        arr =[onpage,onpage+1,onpage+2,onpage+3,onpage+4]; 
    }else if(arr.indexOf(onpage)==-1){
        arr =[onpage-4,onpage-3,onpage-2,onpage-1,onpage];    
    }
}
function nextPage() {
    pagination(++onpage);
}
function previousPage() {
    pagination(--onpage);
}
function firstPage() {
    pagination(1);
}
function lastPage() {
    pagination(lpage);
}
function displayList(num) {
    var from = ((num - 1) * dis);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var p = JSON.parse(this.responseText);
            $(".view-list ").html("");
            lsize = p.length;
            lpage = Math.floor(lsize / dis);
            if((lsize % dis)>0){
            lpage++;
            }
            onpage = Math.floor(num);
            
            paginationActive(onpage);
           

   
            if (onpage >= 2) {
                $(".previous-page, .first-page").removeClass("disabled");
            } else {
                $(".previous-page, .first-page").addClass("disabled");
            }
            if (lpage == onpage) {
                $(".next-page, .last-page").addClass("disabled");
            } else {
                $(".next-page, .last-page").removeClass("disabled");
            }

            for (var i = from; i < from + dis; i++) {
                $(".view-list ").append(card(p[i]));
                star(p[i].id);
            }
        }
    }

    xmlhttp.open("GET", "json/freelancer.json", true);
    xmlhttp.send();


}

function setLocalStorang(id){
    if (typeof(Storage) !== "undefined") {
        sessionStorage.setItem("profile-id-freelancer", id+"");
    } else {
        alert("EORROR")
    }
}
function card(person) {
    var c =
        '<div class="col-xl-3 col-lg-4 col-md-6 col-sm12"><div class="card testimonial-card" style="margin-bottom: 15px"><div class="card-up blue-gradient">' +
        '</div><div class="avatar mx-auto white view zoom"><a href="html/profile-freelancer.html" onclick="setLocalStorang('+person.id+')"><img src="' + person.image + '" class="rounded-circle ">' +
        '</a></div><div class="card-body"><a class="text-dark" href="html/profile-freelancer.html" onclick="setLocalStorang('+person.id+')"><h4 class="card-title"><strong>' + person.last_name + '</strong>' +
        '</h4></a><div class="star-rating "><a>' +
        '<span class="fa fa-' + person.id + ' fa-star-o" data-rating="1"></span>' +
        '<span class="fa fa-' + person.id + ' fa-star-o" data-rating="2"></span>' +
        '<span class="fa fa-' + person.id + ' fa-star-o" data-rating="3"></span>' +
        '<span class="fa fa-' + person.id + ' fa-star-o" data-rating="4"></span>' +
        '<span class="fa fa-' + person.id + ' fa-star-o" data-rating="5"></span>' +
        '<input type="hidden" name="whatever3" class="rating-value" value="4.1"></a></div>' +
        '<h6 class="font-weight-bold indigo-text py-2">' + person.job + '</h6>' +
        '<p class="card-text">' + person.info + '</p>' +
        ' <a class="px-2 fa-lg li-ic"><i class="fa fa-linkedin"></i></a><a class="px-2 fa-lg tw-ic"><i class="fa fa-twitter"></i></a><a class="px-2 fa-lg fb-ic"><i class="fa fa-facebook"></i></a></div></div></div>';
        
    
        return c;
}

function star(id) {
    var $star_rating = $('.star-rating .fa-' + id + '');

    var SetRatingStar = function () {
        return $star_rating.each(function () {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });

    SetRatingStar();
}