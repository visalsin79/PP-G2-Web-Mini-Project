$(document).ready(function () {
    $('.btn-edit').click();
    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });

    SetRatingStar();

    var img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXk5ueutLfi5OWvtbja3d7n6eq1ur2rsbTKztC4vsDGysy2u768wcPR1Nbf4eLY29zGy8zO0dSqEPS1AAAFX0lEQVR4nO2d3ZazKgyGKz+CCDre/81u0W/22I5tFZISnDwHXWvmyHcRkhAg3G4MwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAM83fRM6W/AQsxqDFMk7VTCKMaROnvAUXfVLC+a2XTNMbMP7LtvZuUuMZwaq2sX8Q9INvuS13AZofQLcO2izGtHUp/YR7Kts/UfYvs3Vj6K5PRt7f6FuZxrNNWxdQ+Nc+HcZS2Rteq/DF5K74+Uw39GYHRVOuyVOEPGugPxtfkVdV5gTNdNZaqVZegb6YdK7HUcSeBOYYMpb/9EONJH3M3iqGCUVQZAudRVOQlDjLFyWxGkbq7GZK86JZOldbwmq9MfTO+tIZXaJsvsDG2tIwXqOQ4sYVyzMiehCsd2fwNwkYjxhENGerQevcQNP2pdmACjSstZhcFMwkXaDobB6iw8QTLGiPcLJyRBJM3uFm40JFzp4COdMFQc6d6ghVIMCZmrQr36IklNpChYoVYwNAWXGHzVVrUHSKxuvYKWmYKGwz/QSokBgSBhlSdHzRj+4ZS0Ben9pmOIkvL2jBgTMPGEHI1I4bAxhAqgAeMaUjJ1WDE+4bUSl8D1dgeoVMb1iiudA4XZBb6GiFnWxSScaYaJViQykyRFLZ01vkg2xWssCiXt9LrexoNXoZaIRQtrh/xr5+1XT7zvv7q6Q+sgJGqGGTC4V+oRF2/mniD3luLUHI0f6Gqj7Ez05LJ2SLX3127/g4pwhKRlpECnSzdQikpXbn8aZObgD4xVFrQb4BPfVHzMxHYk3vE/MzC9U9fQp6gpVO/uOPyp6ABT7J7SquKLQk3R3ehUyf9xfVvlMDcCiKXr90B4E+J+tFvrn87L/uGJcVD+vfovKgoa7jOnZOCS0Jl7hdk3FafSn/7MZI7DlRhoiupXSOqEZjY+YN6mHhgOulvJKka/iEu34Fnjv32aPA30pE5k3AKLdwhU5Wu0k5YEWXfdcMybcXdzBaU7V92pHOVedAdtFZut6tg0/azvHrtc4sW4+R8P6s0zTqesvVuGiuefnus3T1t5HrdPf9H/6P0dyCwqhLf3K7TjDbKELN1Wue8991M38ff+Q9npzAOVSuN8y56UbOwEymWf8vO2VDftBRqHrUuijuQ0kSZvXfLgJb+8CNoPYxP4t9rZNv5iX58HEbbN4dG7slwShcUWYvVQ3B9Zi0x0no7EhxKfRtdgmk+QfqJWL4jlDvmVQ5jjA90dqCG4AGM85fGpqNhrfpmO6RLQXFKlm9pHs0TS18TjbULRV3r+4bkAPhiGo+WYbKRhRoMHy+l5WNKFBsD0t3fJ8gPvzCgB4fmP5/RfXLjTYTmYwb6w+c2NvSIdBnvLe2HereGz3jQXT5RHR+AT8qew7Top4lSt3fBkBNu/M/ptw4lEXWfCvpAfhKmR/OpAqBZNwQGK4uDPo2fAc4zEaIrEOWfYRBcav6RPFDgowbOPdgMoF/CgDrfDIgBdTcC5Z5vJqAvYZCIg7+AO/AOfvMOCNNDZTcwJ/ARgDrzPlBzoz/A3FsgFye2QJwK1xNVG10AuIgpCq7oD5D/KA3BUH9P9lQMpG00kvlOBEY3CGiyElSkBlCwyJxBFORttMl0NjUMYVZrvryH/j5H+iDSDvY/JC8yCCekD6S6U4znAHBIzN2wuiEikNgIpYpQsZLYFgynGSIOaX1A6ZS435O0ThwqCYYrKSGR/qpig0lZYRAtsD0hJXOraRrGt6BPCxwqWBluSNiMwmm6iob5Oh0RyZaBn3C+aU89SelKf1ahnirKaCKnW/JWUaG546zCzUWzSjivkGEYhmEYhmEYhmEYhvkk/wFrwFbqWH+JagAAAABJRU5ErkJggg==";
    var ad = "Phnom Penh, Cambodia.";
    var info_text = "Hello,\n" +
        "My name is Kinga, I am a graphic designer with 8 years experience in design, print and photography.\n" +
        "I recently decided to start using my freelancer account I made a few years ago and I'm looking\n" +
        "forward starting my first projects.\n\n" +
        " What can I do for you:\n" +
        "· Layout and creation of magazines/brochures/flyers/posters\n" +
        "· Illustrations\n" +
        "· Promotional materials (business cards, t-shirts)\n" +
        "· Label Design for products\n" +
        "· Brand Identity / Brandbook\n" +
        "· Webdesign\n" +
        "· Photography\n" +
        "· Photo editing/retouching";
    setProfile("Anonymous", "Anonymous", "None", img, ad, info_text);
    setContentDailog();

    $(".btn-edit").click(function () {
        setContentDailog();
    })

    $("#btn-d-save").click(function () {
        setProfile(d_firstname.val(), d_lastname.val(), d_skill.val(), d_image.attr("src"), d_address.val(), d_info.val());
        toastr["success"]("Successfully Save!");
        $('.modal').modal('hide');
    })

    $(".btn-add-ref").click(function () {
        d_get_image_ref.change(function () {
            d_image_ref.attr("src", "../img/" + $(this).val().split('\\').pop());
        })
    })

    $("#btn-d-save-ref").click(function () {
        addRef(d_title_ref.val(), d_info_ref.val(), d_image_ref.attr("src"));
        toastr["success"]("Successfully Add!");
        $('.modal').modal('hide');
        d_title_ref.val("");
        d_info_ref.val("");
        d_image_ref.attr("src", "https://demingacademy.com/wp-content/uploads/2017/12/c1.png");
    })

})




function setProfile(f_name, l_name, job, img, address, info) {
    lastname.text(l_name);
    fname.text(f_name);
    lname.text(l_name);
    skill.text(job);
    profileimag.attr("src", img);
    this.address.text(address),
    this.info.text(info)
}
function setContentDailog(){
    dialogImg(profileimag.attr("src"));
    d_lastname.val(lname.text());
    d_firstname.val(fname.text());
    d_skill.val(skill.text());
    d_address.val(address.text());
    d_info.val(info.text());
    d_get_image.change(function () {
        dialogImg("../img/" + $(this).val().split('\\').pop());
    })
}

var lastname = $("#lastname");
var fname = $("#fname");
var lname = $("#lname");
var profileimag = $("#profileimag");
var skill = $("#skill");
var address = $("#address");
var info = $("#info");

//Dialog
var d_image = $("#edit-img");
var d_get_image = $("#edit-get-img");
var d_lastname = $("#d-lname");
var d_firstname = $("#d-fname");
var d_address = $("#d-address");
var d_skill = $("#d-skill");
var d_info = $("#d-info");

//Dialog - ref
var d_image_ref = $("#edit-img-ref");
var d_get_image_ref = $("#edit-get-img-ref");
var d_title_ref = $("#d-title-ref");
var d_info_ref = $("#d-info-ref");
function dialogImg(img) {
    d_image.attr("src", img);
}



function addRef(title, des, img) {
    var card =
        '<div class="col-md-3 mb-4">' +
        '<div class="card ">' +
        '<img class="card-img-top" src="' + img + '" alt="Card image cap">' +
        '<div class="card-body text-center">' +
        '<h5 class="card-title">' + title + '</h5>' +
        '<p class="card-text">' + des + '</p>' +
        '<a type="button" class="btn-floating btn-danger btn-minus" onclick="removeRef(this)">' +
        '<i class="fa fa-minus" aria-hidden="true"></i>' +
        '</a>' +
        '</div>' +
        '</div>' +
        '</div>';

    $(".ref-card").append(card);
}
function removeRef(t) {

    $(t).parentsUntil(".col-md-3").parent().fadeOut(300, "swing", function () {
        $(t).parentsUntil(".col-md-3").parent().remove();
    });

}
var $star_rating = $('.star-rating .fa');

var SetRatingStar = function () {
    return $star_rating.each(function () {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};

