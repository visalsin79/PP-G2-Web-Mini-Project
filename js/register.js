$(document).ready(function () {
    var reg_email = $("#reg-email");
    var reg_pass = $("#reg-pass");
    var reg_pass_confrim = $("#reg-pass-confrim");
    var reg_btn = $("#reg-btn");
    var model_form = $("#modalLRForm");



    
var user_list = new Array();
var user_store = JSON.parse(localStorage.getItem("useracc"));

if(user_store != null){
    user_list = user_store;
}


// localStorage.removeItem("useracc")
    function UserAcc(email, pass, type) {
        this.email = email;
        this.pass = pass;
        this.type = type
    }


    reg_btn.click(function () {
        var reg_type = $(".reg-type[name='reg_type']:checked").val();
        var em =reg_email.val().toLowerCase();
        if (reg_pass.val() == reg_pass_confrim.val() && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(reg_email.val())) {
            if (user_list.map(function(e) {return e.email}).indexOf(reg_email.val().toLowerCase())) {
                user_list.push(new UserAcc(reg_email.val().toLowerCase(), reg_pass.val(),reg_type));
                localStorage.setItem("useracc", JSON.stringify(user_list));
                regSuccess(reg_type);
                localStorage.setItem("login_acc", em);
            } else {
                alert("Your Email is already registered!");
            }
        } else {
            regFail();
        }



    })

    function regSuccess(type) {
        model_form.modal('hide');
        reg_email.val("").removeClass('valid').siblings().removeClass('active');
        reg_pass.val("").removeClass('valid').siblings().removeClass('active');
        reg_pass_confrim.val("").removeClass('valid').siblings().removeClass('active');

        if(new String(type).valueOf() == new String("work").valueOf() ){
            window.location.replace("profile-freelancer-signup.html");
        }else{
            window.location.replace("profile-project-owner-singup.html");
        }
        

    }
    function regFail() {
        reg_email.addClass('invalid');
        reg_pass.addClass('invalid');
        reg_pass_confrim.addClass('invalid');

    }
})