$(".touch-slider").owlCarousel({
    navigation: true,
    pagination: false,
    slideSpeed: 1000,
    stopOnHover: true,
    autoPlay: true,
    items: 1,
    itemsDesktopSmall: [1024, 1],
    itemsTablet: [600, 1],
    itemsMobile: [479, 1]
});
$('.touch-slider').find('.owl-prev').html('<i class="ti-angle-left"></i>');
$('.touch-slider').find('.owl-next').html('<i class="ti-angle-right"></i>');
$('#new-products').find('.owl-prev').html('<i class="fa fa-angle-left"></i>');
$('#new-products').find('.owl-next').html('<i class="fa fa-angle-right"></i>');
var owl;
$(document).ready(function () {
    owl = $("#owl-demo");
    owl.owlCarousel({
        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        afterInit: afterOWLinit,
        afterUpdate: afterOWLinit
    });

    function afterOWLinit() {
        $('.owl-controls .owl-page').append('<a class="item-link" />');
        var pafinatorsLink = $('.owl-controls .item-link');
        $.each(this.owl.userItems, function (i) {
            $(pafinatorsLink[i]).css({
                'background': 'url(' + $(this).find('img').attr('src') + ') center center no-repeat',
                '-webkit-background-size': 'cover',
                '-moz-background-size': 'cover',
                '-o-background-size': 'cover',
                'background-size': 'cover'
            }).click(function () {
                owl.trigger('owl.goTo', i);
            });
        });
        $('.owl-pagination').prepend('<a href="#prev" class="prev-owl"/>');
        $('.owl-pagination').append('<a href="#next" class="next-owl"/>');
        $(".next-owl").click(function () {
            owl.trigger('owl.next');
        });
        $(".prev-owl").click(function () {
            owl.trigger('owl.prev');
        });
    }
});