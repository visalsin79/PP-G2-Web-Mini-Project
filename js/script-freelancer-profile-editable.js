$(document).ready(function () {

    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });
    
    SetRatingStar();
    

    $.getJSON("../json/freelancer.json", function (result) {
        var p = result[parseInt(id)-1];
        var ad= "Phnom Penh, Cambodia.";
        var info = "Hello,\n"+
        "My name is Kinga, I am a graphic designer with 8 years experience in design, print and photography.\n"+
        "I recently decided to start using my freelancer account I made a few years ago and I'm looking\n"+
        "forward starting my first projects.\n\n"+
        " What can I do for you:\n"+
        "· Layout and creation of magazines/brochures/flyers/posters\n"+
        "· Illustrations\n"+
        "· Promotional materials (business cards, t-shirts)\n"+
        "· Label Design for products\n"+
        "· Brand Identity / Brandbook\n"+
        "· Webdesign\n"+
        "· Photography\n"+
        "· Photo editing/retouching";
        setProfile(p.first_name,p.last_name,p.job,p.image,ad,info);

    });

    $(".btn-edit").click(function(){
        dialogImg(profileimag.attr("src"));
        d_lastname.val(lname.text());
        d_firstname.val(fname.text());
        d_skill.val(skill.text());
        d_info.val(info.text())
        d_address.val(address.text())
        d_get_image.change(function(){
            dialogImg("../img/"+$(this).val().split('\\').pop());
        })
    })

    $("#btn-d-save").click(function(){
        setProfile(d_firstname.val(),d_lastname.val(),d_skill.val(), d_image.attr("src"),d_address.val(),d_info.val());
        toastr["success"]("Successfully Save!");
        $('.modal').modal('hide');
    })

    $(".btn-add-ref").click(function(){
        d_get_image_ref.change(function(){
            d_image_ref.attr("src","../img/"+$(this).val().split('\\').pop());
        })
    })

    $("#btn-d-save-ref").click(function(){
        addRef(d_title_ref.val(),d_info_ref.val(),d_image_ref.attr("src"));
        toastr["success"]("Successfully Add!");
        $('.modal').modal('hide');
        d_title_ref.val("");
        d_info_ref.val("");
        d_image_ref.attr("src","https://demingacademy.com/wp-content/uploads/2017/12/c1.png");
    })

})
// var id = sessionStorage.getItem("profile-id-freelancer");
// if(id == null){id=1}
var id =1;


function setProfile(f_name,l_name,job,img,address,info){
    lastname.text(l_name);
    fname.text(f_name);
    lname.text(l_name);
    skill.text(job);
    profileimag.attr("src",img);
    this.address.text(address),
    this.info.text(info)
}


var lastname = $("#lastname");
var fname = $("#fname");
var lname = $("#lname");
var profileimag = $("#profileimag");
var skill = $("#skill");
var address = $("#address");
var info = $("#info");

//Dialog
var d_image = $("#edit-img");
var d_get_image = $("#edit-get-img");
var d_lastname = $("#d-lname");
var d_firstname = $("#d-fname");
var d_address = $("#d-address");
var d_skill = $("#d-skill");
var d_info = $("#d-info");

//Dialog - ref
var d_image_ref = $("#edit-img-ref");
var d_get_image_ref = $("#edit-get-img-ref");
var d_title_ref = $("#d-title-ref");
var d_info_ref = $("#d-info-ref");
function dialogImg(img){
    d_image.attr("src",img);
}



function addRef(title,des,img){
    var card = 
    '<div class="col-md-3 mb-4">'+
        '<div class="card ">'+
            '<img class="card-img-top" src="'+img+'" alt="Card image cap">'+
            '<div class="card-body text-center">'+
                '<h5 class="card-title">'+title+'</h5>'+
                '<p class="card-text">'+des+'</p>'+
                '<a type="button" class="btn-floating btn-danger btn-minus" onclick="removeRef(this)">'+
                    '<i class="fa fa-minus" aria-hidden="true"></i>'+
                '</a>'+
            '</div>'+
        '</div>'+
    '</div>';

    $(".ref-card").append(card);
}
function removeRef(t){

    $(t).parentsUntil(".col-md-3").parent().fadeOut(300,"swing",function(){
        $(t).parentsUntil(".col-md-3").parent().remove();
    });
    
}
var $star_rating = $('.star-rating .fa');

var SetRatingStar = function () {
    return $star_rating.each(function () {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};

