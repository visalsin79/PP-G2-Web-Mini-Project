$(document).ready(function () {

    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });
    
    SetRatingStar();

})
var id = sessionStorage.getItem("profile-id-freelancer");
if(id == null){id=1}

$.getJSON("../json/freelancer.json", function (result) {
    var p = result[parseInt(id)-1];
    lastname.text(p.last_name);
    fullname.text(p.first_name +" "+ p.last_name);
    skill.text(p.job);
    profileimag.attr("src",p.image);
});

var lastname = $("#lastname");
var profileimag = $("#profileimag");
var fullname = $("#fullname");
var skill = $("#skill");



var $star_rating = $('.star-rating .fa');

var SetRatingStar = function () {
    return $star_rating.each(function () {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};

